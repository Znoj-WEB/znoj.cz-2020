export interface MenuItem {
    name: string;
    path: string;
    component: JSX.Element;
}

export interface ListItem {
    link: string;
    name: string;
    description: string;
    icon: string;
}

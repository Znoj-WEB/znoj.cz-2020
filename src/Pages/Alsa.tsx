import './Contact.css';

import { List, Row } from 'antd';
import React from 'react';

import { getCoveredInCard } from '../Utils/cover';
import { ListItem } from '../Utils/interfaces';

const ticket: Array<ListItem> = [
    {
        link: '/alsa.pdf',
        name: `DOWNLOAD bus ticket`,
        description: 'bus ticket for Daniela Znojova',
        icon: 'bus.svg',
    }
];

function Alsa() {
    return (
        <React.Fragment>
            <Row justify="center">
                <h1>Alsa bus ticket</h1>
            </Row>
            {getCoveredInCard(
                <div>
                    <List
                        itemLayout="vertical"
                        size="large"
                        dataSource={ticket}
                        renderItem={(item) => (
                            <List.Item
                                key={item.name}
                                extra={
                                    <Row align="middle">
                                        <a href={item.link}>
                                            <img
                                                className="contactIcon"
                                                src={`/images/contact/${item.icon}`}
                                                alt="icon"
                                            />
                                        </a>
                                    </Row>
                                }
                            >
                                <List.Item.Meta title={<a href={item.link}>{item.name}</a>} />
                                <a href={item.link}>{item.description}</a>
                            </List.Item>
                        )}
                    />
                </div>,
            )}
        </React.Fragment>
    );
}

export default Alsa;

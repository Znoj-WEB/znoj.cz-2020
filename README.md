### **Description**

Jiri Znoj - personal website

---

### **Link**

served with Firebase:  
[https://znoj.cz](https://znoj.cz/)  
[https://znoj-4b8bc.web.app](https://znoj-4b8bc.web.app/)  
[https://znoj-4b8bc.firebaseapp.com](https://znoj-4b8bc.firebaseapp.com/)

---

### **Technology**

TypeScript, Ant Design, React, Hooks, Redux, Firebase, GitLab CI/CD

---

### **Year**

2020

---

### **Screenshots**

![](./README/main.png)

![](./README/1.png)

![](./README/2.png)

![](./README/3.png)

![](./README/m1.png)
